# DIS Coding Demos
This repo contains all demos I show during the club meetings. If you don't understand something, refer to the code here and ask me on Discord or Matrix.

## Links

[Discord Server (Bridged with Matrix)](https://discord.gg/T3em86Hv)
[Matrix Space (Bridged with Discord)](https://matrix.to/#/!OQcMYHVUNjbVBnwnLV:matrix.org?via=matrix.org)
[Telegram Group (Not bridged with Matrix)](https://t.me/+7tm7qrqjN8NjZDk6)
