import tkinter as tk
from tkinter import messagebox

class App(tk.Tk):
    def __init__(self):
        # Initializes the parent class 
        super().__init__()
        # Config root window
        self.title("Hello, World!")
        self.geometry("200x200")
        self.resizable(True, True)
        # creates the text
        self.label = tk.Label(self, text="Hello, World!")
        self.label.pack()
        # creates the button
        self.button = tk.Button(self, text="Click me!", command=self.click)
        self.button.pack(side="bottom")

    def click(self):
        messagebox.showinfo("Hello, World!", "Hello, World!")

app = App()
app.mainloop()
