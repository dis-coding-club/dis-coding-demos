names_we_know = ["David", "Joe", "Smith"]
def main():
    global names_we_know
    name = input("What is your name? ")
    if name in names_we_know:
        print(f"Hello, {name}! Welcome back!")
    elif name == "":
        print("Hello, nameless person!")
    else:
        print(f"Hello, {name}! Nice to meet you!")
        names_we_know.append(name)

while True:
    main()
