# All whole numbers, positive and negative, are integers
# this_is_integer = 1
# print(type(this_is_integer))

# All decimals are floats
# this_is_float = 1.0
# print(type(this_is_float))

# Anything in quotes is a string
# this_is_string = "foo"
# print(type(this_is_string))

# True and False are booleans
# this_is_boolean = True
# print(type(this_is_boolean))
#
# this_is_tuple = (1, 2, 3)
# print(len(this_is_tuple))
# print(type(this_is_tuple))
# x, y, _ = this_is_tuple
# print(x, y)
#
# this_is_list = [1, 2, 3, "four", 5.5]
# print(type(this_is_list))
# for i in this_is_list:
#     print(f"{i} is of type {type(i)}")
# this_is_list.append(4)
# print(this_is_list)
# this_is_list.pop(3)
# print(this_is_list)
