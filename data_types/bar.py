import random
from re import L

example_list = [1, 2, 3, 4, 5, "six", 7.0, False]
print(example_list)
for i in example_list:
    print(f"{i} is of type {type(i)}")

# List comprehensions

example_list_two = [i for i in range(100) if i % 9 == 0 and i != 0] # numbers divisible by 2 and is not 0
example_list_two = []
for i in range(100):
    if i % 9 == 0 and i != 0:
        example_list_two.append(i)
# % = modulo, finds remainder from division
# example_list_two = [i for i in range(100) if i % 7 == 0] # numbers divisible by 2 and is not 0
print(example_list_two)
#
#
# fruits = ["apple", "banana", "cherry", "kiwi", "mango"]
#
# # If character a is in the word
# newlist = [x for x in fruits if "a" in x]
#
# print(newlist)
#
# # unsorted_list = [5, 2, 1, 4, 3]
# random_list = [random.randint(1, 100) for i in range(100)]
# print(random_list)
# random_list.sort()
# print(random_list)
# # sorted_list = sorted(unsorted_list)
