# def plus_one(x):
#     return x + 1
#
def foo(x: int) -> int:
    return x + 1


def bar(x: int) -> int:
    return x - 1


def baz(x: int) -> int:
    return x * 2


def buzz(x: int) -> int | float:
    return x / 2


plus_one = foo(1)
print(plus_one)
plus_one_float = foo(1.4)
print(f"One float {plus_one_float}")
minus_one = bar(1)
print(minus_one)
times_two = baz(1)
print(times_two)
divide_two = buzz(1)
print(divide_two)


#
# bazz = int(1).__add__(1)
# print(f"bazz is {bazz} and type is {type(bazz)}")  # bazz is 2 and type is <class 'int'> (bazz)
#
# Static typing
def foo_type_checking(x: int) -> int:
    if type(x) == int:
        # if isinstance(x, int):
        return x + 1
    else:
        raise TypeError("x must be an int")


print(foo_type_checking(1))
print(type(foo_type_checking))
print(foo_type_checking.__annotations__)
# foo_type_checking(0.4)


def plus_ten(x):
    return "Output"
    return x + 10


print(plus_ten(1))

# Built in functions

print(pow(2, 3))
print(min(10, 50, 30))
print(max(10, 50, 30))
print(sum([10, 50, 30]))
print(round(10.999, 2))
print(len("hello"))
