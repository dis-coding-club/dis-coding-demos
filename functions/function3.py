from typing import Callable


def say_hello(name: str) -> str:
    return f"Hello, {name}!"


def say_gm(name: str) -> str:
    return f"Good morning, {name}!"


def say_guten_tag(name: str) -> str:
    return f"Guten Tag, {name}!"


def say_ni_hao(name: str) -> str:
    return f"你好, {name}!"

def say_hola(name: str) -> str:
    return f"Hola, {name}!"

# Task: define hello in your native language


def hello_many_people(*names: str, say: Callable[[str], str]) -> None:
    for name in names:
        print(say(name))


hello_many_people("Alice", "Bob", "Eve", say=say_hello)
hello_many_people("Alice", "Bob", "Eve", say=say_gm)
hello_many_people("Alice", "Bob", "Eve", say=say_guten_tag)
hello_many_people("Alice", "Bob", "Eve", say=say_ni_hao)
