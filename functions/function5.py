import math


def cube(side: float) -> tuple[float, float]:
    volume = side**3
    surface_area = 6 * (side**2)
    return volume, surface_area


def rectangular_prism(side1: float, side2: float, side3: float) -> tuple[float, float]:
    volume = side1 * side2 * side3
    surface_area = 2 * (side1 * side2 + side1 * side3 + side2 * side3)
    return volume, surface_area


def cylinder(radius: float, height: float) -> tuple[float, float]:
    volume = math.pi * radius**2 * height
    surface_area = 2 * math.pi * radius * (radius + height)
    return volume, surface_area


def handle_input():
    user_input = input(
        "Enter 'c' for cube, 'r' for rectangular prism, 'h' for cylinder, or 'q' to quit: "
    )
    try:
        match user_input:
            case "c":
                side = float(input("Enter the side length: "))
                volume, surface_area = cube(side)
                print(f"Volume: {volume}, Surface area: {surface_area}")
            case "r":
                side1 = float(input("Enter the first side length: "))
                side2 = float(input("Enter the second side length: "))
                side3 = float(input("Enter the third side length: "))
                volume, surface_area = rectangular_prism(side1, side2, side3)
                print(f"Volume: {volume}, Surface area: {surface_area}")
            case "h":
                radius = float(input("Enter the radius: "))
                height = float(input("Enter the height: "))
                volume, surface_area = cylinder(radius, height)
                print(f"Volume: {volume}, Surface area: {surface_area}")
            case "q":
                print("Thank you for using geometry calculator")
                exit()
            case _:
                print("Invalid input. Please enter 'c', 'r', or 'h'.")
    except ValueError:
        print("Invalid input. Please enter a number.")


def main():
    print("Welcome to the geometry calculator!")
    while True:
        handle_input()


if __name__ == "__main__":
    main()
