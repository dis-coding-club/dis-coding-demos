def add_two(x: int, y: int) -> int:
    return x + y


# Task: define function to do x to power of y

print(add_two(2, 3))
print(add_two(x=2, y=3))


def add_default(x: int, y: int = 2) -> int:
    return x + y


print(add_default(2))
print(add_default(2, 3))


def add_many(*args: int) -> int:
    total = 1
    for x in args:
        total *= x
    return total


print(add_many(1, 2, 3, 4, 5))

# Task: define function to mutiply all numbers


def print_values(**kwargs: str) -> None:
    for key, value in kwargs.items():
        print("The value of {} is {}".format(key, value))


print_values(
    name_5="Remy",
    name_3="Harper",
    name_2="Brian",
    name_1="Alex",
    name_6="Val",
    name_4="Phoenix",
)
