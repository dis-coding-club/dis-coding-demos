from typing import Iterator


def factorial(x: int) -> int:
    if x == 1:
        return 1
    else:
        return x * factorial(x - 1)


print(factorial(5))  # 5 * 4 * 3 * 2 * 1 = 120


def is_even(x: int) -> bool:
   # Stupid use of recursion
    if x == 1:
        return False
    if x == 0:
        return True
    return is_even(x - 2)



def count_up(x: int) -> Iterator[int]:
    i = 0
    while i < x:
        yield i
        i += 1


for i in count_up(5):
    print(i)
#
# for i in range(5):
#     print(i)
