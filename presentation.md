# Welcome to the DIS Coding and Digital Design Club

|--------------------------------------------------|
```py
print("Hello World")

```
|--------------------------------------------------|

```rs
fn main() {
    println!("Hello World");
}
```
|--------------------------------------------------|

```go
package main

import "fmt"

func main() {
	fmt.Printf("Hello World")
}
```
|--------------------------------------------------|

---

# Who am I?
- David
- Student of Grade 11
- Been at DIS since 4th grade
- Hobbyist software developer
    - Python
    - Rust
    - Learning Go and Zig
    - A bit of HTML/CSS out of pure necessity
- Free software enthusiast
- **The** Vim User
- Linux User
- Proud ThinkPad owner with lots of stickers
- The guy who runs the club

---

# Who are you?

---

# What is this club about?

- You will learn coding ~~and be indoctrinated into a cult~~
    - Python
    - A bit of HTML/CSS/TailwindCSS
    - Rust (If we ever get there)
    - Git
    - Bit of command line
    - Software design and best practices
    - If you want, Vim
- ~~Start a GNU/jihad on Apple and Microsoft~~
- Collab on projects
- Game Jam later this year
- A lot of fun
- Chill environment

---

# What should you do?

- Join the Discord/Matrix/Telegram
- Join the Gitlab organisation
- ~~Sacrifice your firstborn child to the gods of Free Software~~
- Install Python
- Install an IDE (VSCodium recommended)
- Install necessary plugins for Python

