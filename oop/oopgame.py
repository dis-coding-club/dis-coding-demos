class Player:
    def __init__(self, name):
        self.name = name
        self.location = "start"


class Room:
    def __init__(self, name, description, paths, endgame):
        self.name = name
        self.description = description
        self.paths = paths
        self.endgame = endgame

    def move(self, direction):
        if direction in self.paths:
            return self.paths[direction]
        else:
            return None


class Game:
    def __init__(self):
        self.player = Player("Player 1")
        self.rooms = {}
        self.setup()

    def setup(self):
        room1 = Room(
            "start",
            "You are in a dark room. There are two doors in front of you.",
            {"north": "room2", "east": "room3"},
            endgame=False,
        )
        room2 = Room(
            "room2",
            "You have entered the room with a door to your west and another door to the east.",
            {"south": "start", "west": "room4", "east": "room3"},
            endgame=False,
        )
        room3 = Room(
            "room3", "You have entered a room with a monster!", {}, endgame=True
        )
        room4 = Room(
            "room4",
            "You open the door and see the outside, you are saved!",
            {},
            endgame=True,
        )

        self.rooms = {"start": room1, "room2": room2, "room3": room3, "room4": room4}

    def play(self):
        current_room = self.rooms[self.player.location]
        print(current_room.description)

        while True:
            direction = input(
                "Enter your move (north, south, east, west, look, locate): "
            )
            match direction:
                case "locate":
                    print(self.player.location)
                case "look":
                    print(current_room.description)
                case "north" | "south" | "east" | "west":
                    new_room = current_room.move(direction)
                    if new_room:
                        self.player.location = new_room
                        current_room = self.rooms[self.player.location]
                        print(current_room.description)
                        if current_room.endgame:
                            print("Game over!")
                            exit()
                    else:
                        print("You can't go that way!")
                case _:
                    print("Invalid direction!")


game = Game()
game.play()
