from abc import abstractmethod, ABC


# Abstract base classes, they are cool!
class MediaPlayer(ABC):
    @abstractmethod
    def play(self):
        pass

    @abstractmethod
    def pause(self):
        pass

    @abstractmethod
    def stop(self):
        pass


class MP3Player(MediaPlayer):
    def play(self):
        print("MP3 player is playing.")

    def pause(self):
        print("MP3 player is paused.")

    def stop(self):
        print("MP3 player is stopped.")


class OGGPlayer(MediaPlayer):
    def play(self):
        print("OGG player is playing.")

    def pause(self):
        print("OGG player is paused.")

    def stop(self):
        print("OGG player is stopped.")


# TODO: This player is missing a method
class BrokenPlayer(MediaPlayer):
    def play(self):
        print("Broken player is playing.")

    def pause(self):
        print("Broken player is paused.")

    def stop(self):
        print("Broken player is stopped.")


players = [MP3Player(), OGGPlayer(), BrokenPlayer()]
for player in players:
    player.play()
    player.pause()
    player.stop()
