class Parent:
    langauges = ["English", "French"]

    def __init__(self, name, age):
        self.name = name
        self.age = age

    def say_hello(self):
        print(
            f"Hello, my name is {self.name}, I am {self.age} years old, and I speak {self.langauges}."
        )


class Child(Parent):
    def __init__(self, name, age):
        super().__init__(name, age)


parent = Parent("John", 50)
parent.say_hello()
child = Child("Jane", 25)
child.langauges.append("Spanish")
child.say_hello()
