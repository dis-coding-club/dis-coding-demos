class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def say_hello(self):
        print(f"Hello, my name is {self.name} and I am {self.age} years old.")


class Employee(Person):
    def __init__(self, name, age, salary, company):
        super().__init__(name, age)
        self._salary = salary  # underscore means private, but you can access it anyways because lol what is privacy
        self.company = company

    def work(self):
        print(f"{self.name} is working at {self.company}.")

    def change_salary(self, new_salary):
        if new_salary < 0:
            raise ValueError("Salary cannot be negative.")
        self._salary = new_salary


employee1 = Employee("David", 17, 10000000, "Microsoft")
employee1.say_hello()
employee1.work()

person = Person("Joe", 20)
# person.work()  # This does not work

print("Is person an instance of Employee?", isinstance(person, Employee))
print("Is person an instance of Person?", isinstance(person, Person))
print("Is employee an instance of Person?", isinstance(employee1, Person))
