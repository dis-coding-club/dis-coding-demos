class Person:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def say_hello(self):
        print(f"Hello, my name is {self.name} and I am {self.age} years old.")


person = Person("Bond", 25)
# person = Person().__init__("John", 25)
print(type(person))
person.say_hello()
print(person.name)
print(person.age)

person.age = 30
print(person.age)
person.name = "Smith"
print(person.name)

print(isinstance(person, Person))
